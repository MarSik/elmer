package org.marsik.elmer.callsign;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Ignore;
import org.junit.Test;

public class CallsignTest {
    @Test
    public void basic() throws Exception{
        new Callsign("OK7MS");
    }

    @Test
    public void basicParsing() throws Exception{
        Callsign callsign = new Callsign("OK7MS");
        assertThat(callsign.prefix())
                .isNotNull()
                .isEqualToIgnoringCase("OK");

        assertThat(callsign.number())
                .isNotNull()
                .isEqualToIgnoringCase("7");

        assertThat(callsign.station())
                .isNotNull()
                .isEqualToIgnoringCase("MS");
    }

    @Test
    public void portable() throws Exception{
        Callsign callsign = new Callsign("OK7MS/p");

        assertThat(callsign.specifier())
                .isNotNull()
                .isEqualToIgnoringCase("/p");
    }

    @Test
    public void portableMobile() throws Exception{
        Callsign callsign = new Callsign("OK7MS/p/m");

        assertThat(callsign.specifier())
                .isNotNull()
                .isEqualToIgnoringCase("/p/m");
    }

    @Test
    public void portableAbroad() throws Exception{
        Callsign callsign = new Callsign("DE/OK7MS/p");

        assertThat(callsign.prefix())
                .isNotNull()
                .isEqualToIgnoringCase("OK");

        assertThat(callsign.operatesFrom())
                .isNotNull()
                .isEqualToIgnoringCase("DE");

        assertThat(callsign.number())
                .isNotNull()
                .isEqualToIgnoringCase("7");

        assertThat(callsign.station())
                .isNotNull()
                .isEqualToIgnoringCase("MS");

        assertThat(callsign.specifier())
                .isNotNull()
                .isEqualToIgnoringCase("/p");
    }

    @Test
    public void portableAbroadHard() throws Exception{
        Callsign callsign = new Callsign("C6/A47MS/p");

        assertThat(callsign.prefix())
                .isNotNull()
                .isEqualToIgnoringCase("A4");

        assertThat(callsign.operatesFrom())
                .isNotNull()
                .isEqualToIgnoringCase("C6");

        assertThat(callsign.number())
                .isNotNull()
                .isEqualToIgnoringCase("7");

        assertThat(callsign.station())
                .isNotNull()
                .isEqualToIgnoringCase("MS");

        assertThat(callsign.specifier())
                .isNotNull()
                .isEqualToIgnoringCase("/p");
    }

    @Test(expected = IllegalArgumentException.class)
    public void basicFailNoNumber() throws Exception {
        new Callsign("OKMS");
    }
}
