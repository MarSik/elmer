package org.marsik.elmer.entity;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;

public class CallsignEntityTest {

    @Test
    public void testMatchesSearchTerm() throws Exception {
        boolean res = CallsignEntity.builder()
                .callsignRe("AAAA")
                .build()
                .matchesSearchTerm("AA");

        assertThat(res)
                .isTrue();

        res = CallsignEntity.builder()
                .callsignRe("AAAA")
                .build()
                .matchesSearchTerm("BA");

        assertThat(res)
                .isFalse();

        res = CallsignEntity.builder()
                .callsignRe("AAAA")
                .build()
                .matchesSearchTerm("AAAAB");

        assertThat(res)
                .isTrue();

        res = CallsignEntity.builder()
                .callsignRe("OK")
                .build()
                .matchesSearchTerm("AOK");

        assertThat(res)
                .isFalse();
    }
}
