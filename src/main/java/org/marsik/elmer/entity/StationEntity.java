package org.marsik.elmer.entity;

import lombok.Builder;
import lombok.Data;

@Data
public class StationEntity {
    String name;

    @Builder
    public StationEntity(String name) {
        this.name = name;
    }
}
