package org.marsik.elmer.entity;

import org.gavaghan.geodesy.GlobalCoordinates;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
public class ZoneDetailEntity {
    // Parent zone
    ZoneDetailEntity parent;

    // Autonomous zone does not use the name of the parent
    boolean autonomous = true;

    // Old instance of this zone (renamed country, new country..)
    ZoneDetailEntity replaces;

    String name;

    String description;

    Date yearValidSince; // the date included

    Date yearValidUntil; // the date included

    int itu;

    int cq;

    String flag;

    String iso3166;

    GlobalCoordinates coordinates;

    DataSource source;

    public GlobalCoordinates getFirstCoordinates() {
        if (coordinates != null) {
            return coordinates;
        }

        if (parent != null) {
            return parent.getFirstCoordinates();
        }

        return null;
    }

    public String getFullName() {
        String fullname = name;

        if (!autonomous && parent != null) {
            fullname = parent.getFullName() + " - " + fullname;
        }

        return fullname;
    }

    public List<String> getAllFlags() {
        List<String> flags = new ArrayList<String>();

        if (flag != null)
            flags.add(flag);

        if (parent != null)
            flags.addAll(parent.getAllFlags());

        if (flags.isEmpty() && replaces != null)
            flags.addAll(replaces.getAllFlags());

        return flags;
    }

    public int getParentCount() {
        int parentCount = 0;
        ZoneDetailEntity p = parent;
        while (p != null) {
            parentCount++;
            p = p.getParent();
        }

        return parentCount;
    }

    public enum DataSource {
        ISO3166,
        ITU,
        PREFIX,
        USER;
    }

    @Builder
    public ZoneDetailEntity(ZoneDetailEntity parent,
            ZoneDetailEntity replaces,
            String name,
            String description,
            Date yearValidSince,
            Date yearValidUntil,
            int itu,
            int cq,
            String flag,
            String iso3166,
            Double latitude,
            Double longitude,
            DataSource source) {
        this.parent = parent;
        this.replaces = replaces;
        this.name = name;
        this.description = description;
        this.yearValidSince = yearValidSince;
        this.yearValidUntil = yearValidUntil;
        this.itu = itu;
        this.cq = cq;
        this.flag = flag;
        this.iso3166 = iso3166.toUpperCase();
        if (latitude != null && longitude != null) {
            this.coordinates = new GlobalCoordinates(latitude, longitude);
        }
        this.source = source;
    }
}
