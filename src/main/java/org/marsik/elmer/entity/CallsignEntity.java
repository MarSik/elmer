package org.marsik.elmer.entity;

import lombok.Builder;
import lombok.Data;

import org.gavaghan.geodesy.GlobalCoordinates;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Data
public class CallsignEntity {
    CallsignEntity parent;

    String callsign;
    Pattern callsignRe;

    static public final Date STILL_VALID = new Date(3999, 1, 1, 0, 0, 0);

    Date yearValidSince; // the date included
    Date yearValidUntil; // the date included

    String flag;

    // is this prefix only?
    boolean prefix;

    ZoneDetailEntity zone;
    StationEntity station;

    GlobalCoordinates coordinates;

    // lower number -> more important result
    int priority;

    public GlobalCoordinates getFirstCoordinates() {
        if (coordinates != null)
            return coordinates;

        if (zone != null)
            return zone.getFirstCoordinates();

        return null;
    }

    public List<String> getAllFlags() {
        List<String> flags = new ArrayList<String>();

        if (flag != null && !flag.isEmpty())
            flags.add(flag);

        if (zone != null)
            flags.addAll(zone.getAllFlags());

        return flags;
    }

    @Builder
    public CallsignEntity(CallsignEntity parent,
            String callsignRe,
            Date yearValidSince,
            Date yearValidUntil,
            String flag, boolean prefix, ZoneDetailEntity zone, Double lat, Double lon,
            StationEntity station) {
        this.parent = parent;
	this.callsign = callsignRe;
        this.callsignRe = Pattern.compile("^(?:"+callsignRe+").*");
        this.yearValidSince = yearValidSince;
        this.yearValidUntil = yearValidUntil;
        this.flag = flag;
        this.prefix = prefix;
        this.zone = zone;
        if (lat != null && lon != null) {
            this.coordinates = new GlobalCoordinates(lat, lon);
        }
        this.station = station;

        if (station != null) {
            priority = 0;
        } else if (zone == null) {
            priority = 1;
        } else {
            priority = (zone.isAutonomous() ? 2 : 3)  + zone.getParentCount();
        }
    }

    public boolean matchesSearchTerm(String prefix) {
        final Matcher matcher = callsignRe.matcher(prefix);
        return matcher.matches() || matcher.hitEnd();
    }

    public String getFullName() {
        if (station != null && station.getName() != null && !station.getName().isEmpty()) {
            return station.getName();
        }

        if (zone != null && zone.getFullName() != null && !zone.getFullName().isEmpty()) {
            return zone.getFullName();
        }

        return "unknown";
    }
}

