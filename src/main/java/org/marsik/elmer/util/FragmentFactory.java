package org.marsik.elmer.util;

import android.support.v4.app.Fragment;

/**
 * Created with IntelliJ IDEA.
 * User: msivak
 * Date: 17.7.13
 * Time: 11:26
 * To change this template use File | Settings | File Templates.
 */
public interface FragmentFactory {
    Fragment instantiate();
}
