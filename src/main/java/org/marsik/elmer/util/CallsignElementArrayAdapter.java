package org.marsik.elmer.util;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.google.common.base.Optional;

import org.gavaghan.geodesy.Ellipsoid;
import org.gavaghan.geodesy.GeodeticCalculator;
import org.gavaghan.geodesy.GeodeticCurve;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.marsik.elmer.R;
import org.marsik.elmer.callsign.CallsignPreferences_;
import org.marsik.elmer.entity.CallsignEntity;

import java.util.List;
import javax.jws.soap.SOAPBinding;

/**
 * http://www.ezzylearning.com/tutorial.aspx?tid=1763429
 */
public class CallsignElementArrayAdapter extends ArrayAdapter<CallsignEntity> {

    int layoutResourceId;
    CallsignPreferences_ preferences;
    private CurrentLocationProvider locationProvider;

    public interface CurrentLocationProvider {
        Optional<GlobalCoordinates> getLocation();
    }

    public CallsignElementArrayAdapter(Context context, int layoutResourceId, List<CallsignEntity> data,
            CallsignPreferences_ preferences, CurrentLocationProvider locationProvider) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.locationProvider = locationProvider;
        this.preferences = preferences;

        // refresh the list when data changes
        setNotifyOnChange(true);
    }

    public CallsignElementArrayAdapter(Context context, int layoutResourceId, CallsignEntity[] data, CurrentLocationProvider locationProvider) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.locationProvider = locationProvider;

        // refresh the list when data changes
        setNotifyOnChange(true);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        CallsignHolder holder = null;

        if(row == null)
        {
            LayoutInflater inflater = ((Activity)getContext()).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            // Prepare the row layout object structure
            holder = new CallsignHolder();
            holder.title = (TextView)row.findViewById(R.id.title);
            holder.callsign = (TextView)row.findViewById(R.id.callsign);
            holder.flag = (ImageView)row.findViewById(R.id.flag);
            holder.direction = (TextView)row.findViewById(R.id.direction);
            row.setTag(holder);
        }
        else
        {
            holder = (CallsignHolder)row.getTag();
        }

        CallsignEntity call = getItem(position);
        holder.callsign.setText(call.getCallsign().toString()
                .toUpperCase());

        String desc = call.getFullName();
        holder.title.setText(desc);

        String direction = "";
        GlobalCoordinates coordinates = call.getFirstCoordinates();
        if (coordinates != null) {
            Optional<GlobalCoordinates> here = locationProvider.getLocation();
            direction = MaidenheadCalculator.toMaidenhead(coordinates);

            if (here.isPresent()) {
                GeodeticCalculator calc = new GeodeticCalculator();
                GeodeticCurve curve = calc.calculateGeodeticCurve(Ellipsoid.WGS84, here.get(), coordinates);
                direction += String.format(" %.0fkm %.0f°",
                        curve.getEllipsoidalDistance() / 1000,
                        curve.getAzimuth());
            }
        }



        // translate the flag iso code to the resource id
        // TODO use preinitialized hashmap to do the translation
        int flagId = 0;
        List<String> flags = call.getAllFlags();
        for (String flagName : flags) {
            int candidateFlagId = getContext().getResources().getIdentifier("flag_" + flagName, "drawable", getContext().getPackageName());
            if (candidateFlagId == 0) continue;
            flagId = candidateFlagId;
            break;
        }

        for (String flagName : flags) {
            direction += flagName.toUpperCase() + " ";
        }

        holder.direction.setText(direction);
        holder.flag.setImageResource(flagId);

        return row;
    }

    // Define the structure needed to hold the objects that are used in the row
    static class CallsignHolder
    {
        ImageView flag;
        TextView title;
        TextView callsign;
        TextView direction;
    }
}
