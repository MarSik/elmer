package org.marsik.elmer.view;

import android.content.Context;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.view.View;

/**
 * http://developer.android.com/training/custom-views/index.html
 * This view will draw a graphical representation of a coax cable
 */
public class CoaxDiagramView extends View {
    public CoaxDiagramView(Context ctx, AttributeSet attrs)
    {
        super(ctx, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }
}
