package org.marsik.elmer.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.View;

/**
 * http://developer.android.com/training/custom-views/index.html
 * This view will draw the smith chart
 */
public class SmithChartView extends View {
    float[] impedanceCircles = new float[] {0.2f, 0.5f, 1f, 2f, 5f, 10f};

    public SmithChartView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int height = canvas.getHeight() - getPaddingTop() - getPaddingBottom();
        int cy = getPaddingBottom() + height / 2;

        int width = canvas.getWidth() - getPaddingLeft() - getPaddingRight();
        int cx = getPaddingLeft() + width / 2;

        int radius = Math.min(cx, cy);

        Paint brush = new Paint(Paint.ANTI_ALIAS_FLAG);
        brush.setColor(Color.WHITE);
        brush.setStyle(Paint.Style.STROKE);
        brush.setStrokeWidth(0);

        canvas.drawCircle(cx, cy, radius, brush);
        canvas.drawLine(cx - radius, cy, cx + radius, cy, brush);

        // http://stackoverflow.com/questions/9285450/on-android-how-do-i-make-oddly-shaped-clipping-areas
        Path path = new Path();
        path.addCircle(cx, cy , radius+1, Path.Direction.CW);
        canvas.save();
        canvas.clipPath(path);

        // constant resistance circles
        for(float r: impedanceCircles){
            float circleRadius = radius / (1 + r);
            canvas.drawCircle(width - circleRadius, cy, circleRadius, brush);
        }

        // constant reactance circles
        for(float r: impedanceCircles){
            float circleRadius = radius / r;
            canvas.drawCircle(width, cy + circleRadius, circleRadius, brush);
            canvas.drawCircle(width, cy - circleRadius, circleRadius, brush);
        }
        canvas.restore();
    }
}
