package org.marsik.elmer.callsign;

import org.androidannotations.annotations.sharedpreferences.DefaultString;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref
public interface CallsignPreferences {
    String maidenheadLocation();
}
