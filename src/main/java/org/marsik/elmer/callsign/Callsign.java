package org.marsik.elmer.callsign;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * valid callsign regular expression
 * examples:
 * VK4/OK7MS-5/p
 * OK7MS/9A/mm
 * F/OK7MS-9
 */
public class Callsign {
    public final static Pattern CALLSIGN_RE = Pattern.compile("^(?:([A-Z0-9]{1,3})/)?([A-Z0-9]{2,3})([0-9])([A-Z0-9]+)?(-[0-9]+)?((?:/(?:p|m|mm|qrp))*)$", Pattern.CASE_INSENSITIVE);

    private final String callsign;

    private final String prefix;
    private final String number;
    private final String station;
    private final String operatesFrom;
    private final String zone;
    private final String specifier; // portable, mobile, ....

    private Callsign(String term, Void search) {
        prefix = term;
        number = "";
        station = "";
        callsign = term;
        operatesFrom = null;
        zone = null;
        specifier = null;
    }

    public Callsign(String callsign) {
        final Matcher matcher = CALLSIGN_RE.matcher(callsign);
        if (!matcher.matches()) {
            throw new IllegalArgumentException(callsign);
        }

        this.callsign = callsign;
        this.operatesFrom = matcher.group(1);
        this.prefix = matcher.group(2);
        this.number = matcher.group(3);
        this.station = matcher.group(4);
        this.zone = matcher.group(5);
        this.specifier = matcher.group(6);
    }

    public String callsign() {
        return callsign;
    }

    public String prefix() {
        return prefix;
    }

    public String number() {
        return number;
    }

    public static Callsign searchPrefix(String term) {
        return new Callsign(term, null);
    }

    public String station() {
        return station;
    }

    public String operatesFrom() {
        return operatesFrom;
    }

    public String zone() {
        return zone;
    }

    public String specifier() {
        return specifier;
    }
}
