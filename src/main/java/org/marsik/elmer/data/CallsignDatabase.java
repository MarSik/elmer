package org.marsik.elmer.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.marsik.elmer.callsign.Callsign;
import org.marsik.elmer.entity.CallsignEntity;
import org.marsik.elmer.entity.StationEntity;
import org.marsik.elmer.entity.ZoneDetailEntity;

import android.util.Log;
import com.google.common.base.Predicate;
import com.google.common.collect.FluentIterable;

@EBean(scope = EBean.Scope.Singleton)
public class CallsignDatabase {
    private final List<CallsignEntity> db = new ArrayList<>();

    @Bean
    ZoneDatabase zoneDatabase;

    @AfterInject
    public void loadDatabase() {
        String line;
        final int ZONE_TABLE_COLUMNS = 10;
        final SimpleDateFormat dateParser = new SimpleDateFormat("yyyy-MM-dd");

        BufferedReader raw = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/prefixes")));

        try {
            int lineCount = 0;

            while ((line = raw.readLine()) != null) {
                lineCount++;

                String[] parts = line.split(";", ZONE_TABLE_COLUMNS);
                ZoneDetailEntity zone = null;
                Date until = null;
                Date since = null;
                Double lat = null;
                Double lon = null;

                // Some zones have internal identifier
                zone = zoneDatabase.getZone("$" + String.valueOf(lineCount));

                // Find the explicit zone parent, if there is any,
                // but use it only if there was no matching internal zone
                final String zoneCode = parts[1].trim().toLowerCase();
                if (zone == null && !zoneCode.isEmpty()) {
                    zone = zoneDatabase.getZone(zoneCode);
                }

                // Use the zone that matches the ISO code or previously
                // found zone if no ISO zone matches
                final String isoCode = parts[7].trim().toLowerCase();
                if (!isoCode.isEmpty()) {
                    zone = zoneDatabase.getZone(isoCode, zone);
                }

                if (!parts[4].trim().isEmpty()) {
                    try {
                        since = dateParser.parse(parts[4]);
                    } catch (ParseException e) {
                        Log.e(CallsignDatabase.class.getName(), "Can't parse date", e);
                    }
                }

                if (!parts[5].trim().isEmpty()) {
                    try {
                        until = dateParser.parse(parts[5]);
                    } catch (ParseException e) {
                        Log.e(CallsignDatabase.class.getName(), "Can't parse date", e);
                    }
                }

                // coordinates of station, do not save if the coordinates belong to a zone
                if (zone == null && !parts[8].equals("") && !parts[9].equals("")) {
                    lat = Double.parseDouble(parts[8]);
                    lon = Double.parseDouble(parts[9]);
                }

                // register
                String callRe = parts[0].trim().toLowerCase();
                CallsignEntity entity = CallsignEntity.builder()
                        .callsignRe(callRe.toLowerCase())
                        .prefix(zone == null)
                        .zone(zone)
                        .station(zone == null ? StationEntity.builder().name(parts[2].trim()).build() : null)
                        .yearValidSince(since)
                        .yearValidUntil(until)
                        .lat(lat)
                        .lon(lon)
                        .build();

                db.add(entity);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<CallsignEntity> findMatchingCallsigns(final Callsign callsign) {
        return FluentIterable.from(db)
                .filter(new Predicate<CallsignEntity>() {
                    @Override
                    public boolean apply(CallsignEntity callsignEntity) {
                        return callsignEntity.matchesSearchTerm(callsign.prefix() + callsign.number());
                    }
                })
                .toSortedList(new Comparator<CallsignEntity>() {
                    @Override
                    public int compare(CallsignEntity lhs, CallsignEntity rhs) {
                        return lhs.getPriority() - rhs.getPriority();
                    }
                });
    }
}
