package org.marsik.elmer.data;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

import org.androidannotations.annotations.AfterInject;
import org.androidannotations.annotations.EBean;
import org.marsik.elmer.R;
import org.marsik.elmer.entity.ZoneDetailEntity;

import android.content.Context;

@EBean(scope = EBean.Scope.Singleton)
public class ZoneDatabase {
    private final Map<String, ZoneDetailEntity> db = new HashMap<>();

    @AfterInject
    public void loadDatabase() {
        final int ZONE_TABLE_COLUMNS = 10;
        String line;

        BufferedReader raw = new BufferedReader(new InputStreamReader(getClass().getResourceAsStream("/prefixes")));

        int lineCount = 0;

        try {
            while((line = raw.readLine()) != null) {
                lineCount++;
                String[] parts = line.split(";", ZONE_TABLE_COLUMNS);

                final String isoCode = parts[7].trim().toLowerCase();
                final String parentZone = parts[1].trim().toLowerCase();

                if (isoCode.isEmpty() && parentZone.isEmpty()) continue;
                if (!isoCode.isEmpty() && db.containsKey(isoCode)) continue;

                Double lat = null;
                Double lon = null;

                // coordinates
                if (!parts[8].equals("") && !parts[9].equals("")) {
                    lat = Double.parseDouble(parts[8]);
                    lon = Double.parseDouble(parts[9]);
                }

                ZoneDetailEntity entity = ZoneDetailEntity.builder()
                        .flag(isoCode)
                        .iso3166(isoCode.isEmpty() ? "$" + String.valueOf(lineCount) : isoCode)
                        .source(ZoneDetailEntity.DataSource.ISO3166)
                        .latitude(lat)
                        .longitude(lon)
                        .build();

                if (isoCode.indexOf('-') >= 0) {
                    entity.setAutonomous(false);

                    if (parts[6].length() > 0) {
                        entity.setName(parts[6]);
                    }
                    else {
                        entity.setName(parts[2]);
                    }
                }
                else {
                    entity.setName(parts[2]);
                }

                if (!parentZone.isEmpty()) {
                    entity.setParent(db.get(parentZone));
                }

                addZone(entity);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addZone(ZoneDetailEntity entity) {
        final String key = entity.getIso3166().toLowerCase();
        db.put(key, entity);
    }

    public ZoneDetailEntity getZone(String isoCode) {
        return db.get(isoCode.toLowerCase());
    }

    public ZoneDetailEntity getZone(String isoCode, ZoneDetailEntity def) {
        return db.containsKey(isoCode.toLowerCase()) ? db.get(isoCode.toLowerCase()) : def;
    }
}
