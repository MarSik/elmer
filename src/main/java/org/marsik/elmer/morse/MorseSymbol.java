package org.marsik.elmer.morse;

public class MorseSymbol {
    private final int lengthMs;
    private final int pitchHz;
    private final String display;
    private int index;

    public MorseSymbol(int lengthMs, int pitchHz, String display) {
        this.lengthMs = lengthMs;
        this.pitchHz = pitchHz;
        this.display = display;
        this.index = 0;
    }

    public int getIndex() {
        return index;
    }

    public int getLengthMs() {
        return lengthMs;
    }

    public int getPitchHz() {
        return pitchHz;
    }

    public String getDisplay() {
        return display;
    }

    public MorseSymbol index(int index) {
        this.index = index;
        return this;
    }
}
