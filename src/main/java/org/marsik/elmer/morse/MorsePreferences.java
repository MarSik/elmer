package org.marsik.elmer.morse;

import org.androidannotations.annotations.sharedpreferences.DefaultInt;
import org.androidannotations.annotations.sharedpreferences.SharedPref;

@SharedPref
public interface MorsePreferences {
    @DefaultInt(20)
    int wpm();

    @DefaultInt(12)
    int spaceWpm();

    @DefaultInt(2)
    int kochLevel();

    @DefaultInt(550)
    int pitch();
}
