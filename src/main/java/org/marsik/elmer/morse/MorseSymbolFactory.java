package org.marsik.elmer.morse;

public class MorseSymbolFactory {
    private static final int PARIS = 50;

    private final int WPM;
    private final int fWPM;

    private int pitch;

    public MorseSymbolFactory(int WPM, int fWPM) {
        this.WPM = WPM;
        this.fWPM = fWPM;
        this.pitch = 600;
    }

    public int getPitch() {
        return pitch;
    }

    public void setPitch(int pitch) {
        this.pitch = pitch;
    }

    private int tickMs() {
        return (60 * 1000) / (WPM * PARIS);
    }

    private int farnsworthSpaceMs() {
        return (60 * 1000) / (fWPM * PARIS);
    }

    public MorseSymbol dit(final String display) {
        return new MorseSymbol(tickMs(), pitch, display);
    }

    public MorseSymbol dah(final String display) {
        return new MorseSymbol(3 * tickMs(), pitch, display);
    }

    public MorseSymbol separator(final String display) {
        return new MorseSymbol(tickMs(), 0, display);
    }

    // Assumes separator was already used - total 3
    public MorseSymbol letterend(final String display) {
        return new MorseSymbol(2 * farnsworthSpaceMs(), 0, display);
    }

    // Assumes separator and letterend was already used - total 7
    public MorseSymbol space() {
        return new MorseSymbol(4 * farnsworthSpaceMs(), 0, "");
    }
}
