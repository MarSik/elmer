package org.marsik.elmer.morse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.commons.lang3.RandomStringUtils;

public class Morse {
    public interface Symbol {
        MorseSymbol symbol(MorseSymbolFactory factory);
    }

    public interface Consumer {
        void symbol(MorseSymbol symbol);
    }

    public static void encode(List<String> str, MorseSymbolFactory factory, Consumer consumer) {
        int index = 0;
        for (String c: str) {
            c = c.toUpperCase();
            List<Symbol> symbols = table.get(c);
            if (symbols == null) {
                continue;
            }

            for (Symbol s: symbols) {
                consumer.symbol(s.symbol(factory).index(index));
            }

            index++;

            consumer.symbol(factory.letterend("").index(index));
        }
    }

    private static Map<String, List<Symbol>> table = new HashMap<>();

    public static String[] KOCH_ORDER = {"K", "M", "R", "S", "U", "A", "P", "T", "L", "O",
            "W", "I", ".", "N", "J", "E", "F", "0", "Y", "V", "G", "5", "/", "Q", "9", "Z", "H", "3", "8",
            "B", "?", "4", "2", "7", "C", "1", "D", "6", "X", "=", "SK", "+"};

    static {
        prepare(' ', " ");
        prepare('/', "-..-.");
        prepare('?', "..--..");
        prepare('.', ".-.-.-");
        prepare('=', "-...-");
        prepare('+', ".-.-.");

        prepare('A', ".-");
        prepare('B', "-...");
        prepare('C', "-.-.");
        prepare('D', "-..");
        prepare('E', ".");
        prepare('F', "..-.");
        prepare('G', "--.");
        prepare('H', "....");
        prepare('I', "..");
        prepare('J', ".---");
        prepare('K', "-.-");
        prepare('L', ".-..");
        prepare('M', "--");
        prepare('N', "-.");
        prepare('O', "---");
        prepare('P', ".--.");
        prepare('Q', "--.-");
        prepare('R', ".-.");
        prepare('S', "...");
        prepare('T', "-");
        prepare('U', "..-");
        prepare('V', "...-");
        prepare('W', ".--");
        prepare('X', "-..-");
        prepare('Y', "-.--");
        prepare('Z', "--..");

        prepare('0', "-----");
        prepare('1', ".----");
        prepare('2', "..---");
        prepare('3', "...--");
        prepare('4', "....-");
        prepare('5', ".....");
        prepare('6', "-....");
        prepare('7', "--...");
        prepare('8', "---..");
        prepare('9', "----.");

        prepare("SK", "...-.-");
    }

    private static void prepare(char letter, String code) {
        prepare(String.valueOf(letter), code);
    }

    private static void prepare(String letter, String code) {
        List<Symbol> symbols = new ArrayList<>();
        for (char c: code.toCharArray()) {
            switch (c) {
                case ' ': symbols.add(space(letter)); break;
                case '.': symbols.add(dit(letter)); break;
                case '-': symbols.add(dah(letter)); break;
            }
            symbols.add(separator(letter));
        }
        table.put(letter.toUpperCase(), symbols);
    }

    private static Symbol dit(final String display) {
        return new Symbol() {
            @Override
            public MorseSymbol symbol(MorseSymbolFactory factory) {
                return factory.dit(display);
            }
        };
    }

    private static Symbol dah(final String display) {
        return new Symbol() {
            @Override
            public MorseSymbol symbol(MorseSymbolFactory factory) {
                return factory.dah(display);
            }
        };
    }

    private static Symbol separator(final String display) {
        return new Symbol() {
            @Override
            public MorseSymbol symbol(MorseSymbolFactory factory) {
                return factory.separator(display);
            }
        };
    }

    private static Symbol space(final String display) {
        return new Symbol() {
            @Override
            public MorseSymbol symbol(MorseSymbolFactory factory) {
                return factory.letterend(display);
            }
        };
    }

    public static List<String> prepareKochMessage(int level) {
        Random randomGenerator = new Random();
        ArrayList<String> letterGroups = new ArrayList<>();
        for (int i = 0; i < 60; i++) {
            letterGroups.add(KOCH_ORDER[randomGenerator.nextInt(Math.max(2, Math.min(level, KOCH_ORDER.length)))]);
        }
        return letterGroups;
    }
}
