package org.marsik.elmer.morse;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class MorseEvaluation extends ArrayList<Boolean> {
    public MorseEvaluation(int capacity) {
        super(capacity);
    }

    public MorseEvaluation() {
    }

    public MorseEvaluation(Collection<? extends Boolean> collection) {
        super(collection);
        for (Boolean item: collection) {
            elementUpdated(item, 1);
        }
    }

    private void elementUpdated(Boolean item, int direction) {
        if (item != null) {
            if (item) {
                goodCount += direction;
            } else {
                badCount += direction;
            }

            for (ScoreChanged listener: listeners) {
                listener.scoreChanged(goodCount, badCount, size());
            }
        }
    }

    private int goodCount = 0;
    private int badCount = 0;
    private Set<ScoreChanged> listeners = new HashSet<>();

    @Override
    public boolean add(Boolean object) {
        elementUpdated(object, 1);

        return super.add(object);
    }

    @Override
    public Boolean set(int index, Boolean object) {
        elementUpdated(get(index), -1);
        elementUpdated(object, 1);

        return super.set(index, object);
    }

    public void addScoreListener(ScoreChanged listener) {
        listeners.add(listener);
    }

    public int getGoodCount() {
        return goodCount;
    }

    public int getBadCount() {
        return badCount;
    }

    public interface ScoreChanged {
        void scoreChanged(int good, int bad, int size);
    }
}
