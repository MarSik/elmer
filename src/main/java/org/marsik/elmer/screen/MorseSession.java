package org.marsik.elmer.screen;

import java.util.AbstractList;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import android.content.Intent;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.joanzapata.iconify.widget.IconTextView;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.UiThread;
import org.androidannotations.annotations.ViewById;
import org.marsik.elmer.R;
import org.marsik.elmer.morse.Morse;
import org.marsik.elmer.morse.MorseSymbol;
import org.marsik.elmer.morse.MorseSymbolFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@EActivity(R.layout.morse_session)
public class MorseSession extends FragmentActivity {
    private static final Logger log = LoggerFactory.getLogger(MorseSession.class);

    public static final String SPEED = "morse.speed";
    public static final String APPARENT_SPEED = "morse.farnsworthSpeed";
    public static final String PITCH = "morse.pitch";
    public static final String TEXT = "morse.text";

    @ViewById(R.id.morse_letter)
    TextView morseLetter;

    @ViewById(R.id.score_hint)
    IconTextView scoreHint;

    @ViewById(R.id.start_hint)
    IconTextView startHint;

    MorseSymbolFactory factory;
    AudioTrack at;
    List<String> text;

    ProgressBar progressBar;

    @Override
    protected void onDestroy() {
        if (at.getState() == AudioTrack.PLAYSTATE_PLAYING) {
            at.pause();
        }

        at.flush();
        at.release();

        super.onDestroy();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        factory = new MorseSymbolFactory(getWpm(), getSpaceWpm());
        at = prepareAudioTrack(20 * factory.dit("").getLengthMs());
        at.play();

        if (getIntent().hasExtra(TEXT)) {
            text = getIntent().getStringArrayListExtra(TEXT);
        } else {
            text = asList("Hello world");
        }

        progressBar = actionBarProgress();
    }

    private ProgressBar actionBarProgress() {
        // Based on: http://stackoverflow.com/questions/13934010/progressbar-under-action-bar
        // create new ProgressBar and style it
        final ProgressBar progressBar = new ProgressBar(this, null, android.R.attr.progressBarStyleHorizontal);
        progressBar.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, 24));
        progressBar.setProgress(65);

        // retrieve the top view of our application
        final FrameLayout decorView = (FrameLayout) getWindow().getDecorView();
        decorView.addView(progressBar);

        // Here we try to position the ProgressBar to the correct position by looking
        // at the position where content area starts. But during creating time, sizes
        // of the components are not set yet, so we have to wait until the components
        // has been laid out
        // Also note that doing progressBar.setY(136) will not work, because of different
        // screen densities and different sizes of actionBar
        ViewTreeObserver observer = progressBar.getViewTreeObserver();
        observer.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                View contentView = decorView.findViewById(android.R.id.content);
                progressBar.setY(contentView.getY() - 10);

                ViewTreeObserver observer = progressBar.getViewTreeObserver();
                observer.removeGlobalOnLayoutListener(this);
            }
        });

        return progressBar;
    }

    private List<String> asList(final String string) {
        return new AbstractList<String>() {
            public int size() { return string.length(); }
            public String get(int index) { return String.valueOf(string.charAt(index)); }
        };
    }

    @Click(R.id.b_quit)
    public void quitSession() {
        finish();
    }

    @Click(R.id.b_check)
    public void checkSession() {
        if (at.getState() == AudioTrack.PLAYSTATE_PLAYING) {
            at.pause();
        }

        Intent intent = new Intent(this, MorseCheck_.class);

        final ArrayList<String> message = new ArrayList<>(text);
        intent.putStringArrayListExtra(MorseCheck.MESSAGE, message);

        startActivityForResult(intent, 0);
    }

    @Click(R.id.b_resume)
    public void resume() {
        startHint.setVisibility(View.INVISIBLE);

        if (at.getState() == AudioTrack.PLAYSTATE_PLAYING) {
            return;
        } else if (at.getState() == AudioTrack.PLAYSTATE_PAUSED) {
            at.play();
        } else {
            at.play();
            setMorseProgress(0, text.size());
            playSequence(factory, text);
        }
    }

    @Click(R.id.b_pause)
    public void pauseSession() {
        at.pause();
    }

    private int getSpaceWpm() {
        return getIntent().getIntExtra(APPARENT_SPEED, 15);
    }

    private int getWpm() {
        return getIntent().getIntExtra(SPEED, 15);
    }

    @UiThread
    public void showLetter(String value) {
        morseLetter.setText(value);
    }

    private AudioTrack prepareAudioTrack(int maxLengthMs) {
        AudioTrack at;
        int sample_rate = AudioTrack.getNativeOutputSampleRate(AudioManager.STREAM_MUSIC);
        int bufsizsamps = msToSamples(maxLengthMs, sample_rate);
        int bufsizbytes = bufsizsamps * (Short.SIZE / 8);
        int minBuffer = AudioTrack.getMinBufferSize(sample_rate, AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT);


        at = new AudioTrack(AudioManager.STREAM_MUSIC,
                sample_rate,
                AudioFormat.CHANNEL_OUT_MONO,
                AudioFormat.ENCODING_PCM_16BIT,
                Math.max(bufsizbytes, minBuffer),
                AudioTrack.MODE_STREAM);

        at.setStereoVolume(1.0f, 1.0f);
        at.setPlaybackHeadPosition(0);
        at.setPlaybackRate(sample_rate);

        return at;
    }

    AudioTrack getAudioTrack() {
        return at;
    }

    private static double twopi = 8.*Math.atan(1.);

    private static class DisplayUpdate {
        public DisplayUpdate(int playbackHead, String letter, int progress) {
            this.playbackHead = playbackHead;
            this.letter = letter;
            this.progress = progress;
        }

        int playbackHead;
        String letter;
        int progress;
    }

    @Background
    public void playSequence(MorseSymbolFactory factory, final List<String> s) {
        final Queue<MorseSymbol> morseQueue = new LinkedList<>();
        final Queue<DisplayUpdate> displayQueue = new LinkedList<>();

        Morse.encode(s, factory, new Morse.Consumer() {
            @Override
            public void symbol(MorseSymbol symbol) {
                morseQueue.add(symbol);
            }
        });

        AudioTrack at = getAudioTrack();
        at.setPositionNotificationPeriod(msToSamples(factory.dit("").getLengthMs(), at.getSampleRate()));
        at.setPlaybackPositionUpdateListener(new AudioTrack.OnPlaybackPositionUpdateListener() {
            @Override
            public void onMarkerReached(AudioTrack track) {
                // nothing
            }

            @Override
            public void onPeriodicNotification(AudioTrack track) {
                if (displayQueue.isEmpty()) {
                    getAudioTrack().setPositionNotificationPeriod(0);
                    return;
                }

                while (!displayQueue.isEmpty()
                        && getAudioTrack().getPlaybackHeadPosition() >= displayQueue.peek().playbackHead) {
                    final DisplayUpdate update = displayQueue.remove();
                    showLetter(update.letter);
                    setMorseProgress(update.progress, s.size());
                }
            }
        });

        int sequence = at.getPlaybackHeadPosition();

        while (true) {
            int len = enqueueNextSymbol(getAudioTrack(), sequence, morseQueue, displayQueue);
            if (len <= 0) {
                break;
            }

            sequence += len;
        }
    }

    @UiThread
    public void setMorseProgress(int current, int max) {
        progressBar.setMax(max);
        progressBar.setProgress(current);
    }

    private int enqueueNextSymbol(AudioTrack at, int sequence,
            Queue<MorseSymbol> queue, Queue<DisplayUpdate> displayQueue) {
        if (queue.isEmpty()) {
            log.info("Morse queue empty.");
            return 0;
        }

        MorseSymbol symbol = queue.remove();
        log.info("Dequeued '{}' symbol {} Hz duration {} ms.", symbol.getDisplay(), symbol.getPitchHz(), symbol.getLengthMs());

        try {
            int len = playSymbol(at, symbol.getPitchHz(), symbol.getLengthMs());
            displayQueue.add(new DisplayUpdate(sequence + len, symbol.getDisplay(), symbol.getIndex()));
            log.info("Adding display of {} at time {}", symbol.getDisplay(), sequence + len);
            return len;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        return 0;
    }

    static int msToSamples(int ms, int sample_rate) {
        return (ms * sample_rate) / 1000;
    }

    private int playSymbol(AudioTrack at, int pitch, int ms) throws InterruptedException {
        int sample_rate = at.getSampleRate();
        int bufsizsamps = msToSamples(ms, sample_rate);
        short[] buffer = new short[bufsizsamps];

        log.info("Morse symbol {} Hz, {} ms, sample rate {}, buffer len {} bytes, {} samples",
                pitch, ms, sample_rate, buffer.length * Short.SIZE / 8, buffer.length);
        generateSamples(pitch, sample_rate, buffer);

        final int writtenBytes = at.write(buffer, 0, bufsizsamps);

        log.info("Morse Bytes written {}", writtenBytes);
        return writtenBytes;
    }

    private void generateSamples(int pitch, int sample_rate, short[] bufferint) {
        int amp = Short.MAX_VALUE - 2;
        double ph = 0.0;

        for (int i=0; i < bufferint.length; i++) {
            // TODO compute the decay length with low pass characteristics in mind
            bufferint[i] = (short) (decayMultiplier(sample_rate / 50, i, bufferint.length) * amp * Math.sin(ph));
            ph += twopi * (double) pitch / sample_rate;
        }
    }

    private double decayMultiplier(int size, int idx, int total) {
        if (size * 2 > total / 5) {
            size = total / 10;
        }

        if (idx < size) return computeDecayMultiplier(idx, size);
        else if (total - size < idx) return computeDecayMultiplier(total - idx, size);
        else return 1.0f;
    }

    private double computeDecayMultiplier(int idx, int size) {
        return Math.sin(idx * twopi / (4*size));
    }
}
