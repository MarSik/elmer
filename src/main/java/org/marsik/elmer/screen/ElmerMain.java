package org.marsik.elmer.screen;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import org.marsik.elmer.R;

public class ElmerMain extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
    }

    public void startMorse(View view) {
        Intent intent = new Intent(this, MorseDashboard_.class);
        startActivity(intent);
    }

    public void startCallsignLookup(View view) {
        Intent intent = new Intent(this, CallsignLookup_.class);
        startActivity(intent);
    }
}
