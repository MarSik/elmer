package org.marsik.elmer.screen;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicLong;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.SystemService;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.gavaghan.geodesy.GlobalCoordinates;
import org.marsik.elmer.R;
import org.marsik.elmer.callsign.Callsign;
import org.marsik.elmer.callsign.CallsignPreferences_;
import org.marsik.elmer.data.CallsignDatabase;
import org.marsik.elmer.entity.CallsignEntity;
import org.marsik.elmer.util.CallsignElementArrayAdapter;
import org.marsik.elmer.util.MaidenheadCalculator;

import android.app.Activity;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import com.google.common.base.Optional;

/**
 * http://developer.android.com/training/keyboard-input/style.html
 * valid callsign regular expression
 * examples:
 * VK4/OK7MS-5/p
 * OK7MS/9A/mm
 * F/OK7MS-9
 * ([A-Z0-9]{1,3}/)?[A-Z0-9]{2,3}[A-Z0-9]+(/[A-Z0-9]{1,3})?(-[0-9]+)?(/(p|m|mm|qrp))*
 *
 * TODO
 * - QRZ lookup?
 * - eQSL.cc user list download
 * - LoTW?
 */
@EActivity(R.layout.callsign_lookup)
public class CallsignLookup extends Activity implements SearchCallback, CallsignElementArrayAdapter.CurrentLocationProvider {
    @Bean
    CallsignDatabase callsignDatabase;

    @Pref
    CallsignPreferences_ preferences;

    @SystemService
    LocationManager locationManager;
    DummyLocationListener locationListener;

    private Location currentLocation;

    private class DummyLocationListener implements LocationListener {
        public void onLocationChanged(Location location) {
            if (isBetterLocation(location, currentLocation)) {
                currentLocation = location;

                String candidateMaidenhead = MaidenheadCalculator.toMaidenhead(
                        new GlobalCoordinates(currentLocation.getLatitude(),
                                currentLocation.getLongitude()));

                if (!candidateMaidenhead.equals(preferences.maidenheadLocation().get())) {
                    preferences.edit()
                            .maidenheadLocation().put(candidateMaidenhead)
                            .apply();
                }
            }
        }

        public void onStatusChanged(String provider, int status, Bundle extras) {}

        public void onProviderEnabled(String provider) {}

        public void onProviderDisabled(String provider) {}
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        locationListener = new DummyLocationListener();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Refresh once in 10 minutes or 500 meters
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 600000, 500, locationListener);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 600000, 500, locationListener);

        // Load the last known location
        locationListener.onLocationChanged(locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER));
        locationListener.onLocationChanged(locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER));
    }

    @Override
    protected void onPause() {
        locationManager.removeUpdates(locationListener);

        super.onPause();
    }

    @AfterViews
    public void initUiElements() {
        // Register search button callback
        EditText editText = (EditText) findViewById(R.id.search);
        editText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    searchPrefixes(v.getText().toString());
                    handled = true;
                }
                return handled;
            }
        });

        // Filter invalid callsign characters
        editText.setFilters(new InputFilter[] {callsignInputFilterBuilder()});

        // Add text changed callback
        editText.addTextChangedListener(new SearchFieldChanged(this));

        // Create results list
        ListView resultView = (ListView)findViewById(R.id.results);
        List<CallsignEntity> callsignList = new ArrayList<CallsignEntity>();

        CallsignElementArrayAdapter adapter = new CallsignElementArrayAdapter(this,
                R.layout.callsign_element,
                callsignList,
                preferences,
                this);

        resultView.setAdapter(adapter);
    }

    // sequential number to protect the result list from overwriting by an older
    // but slowly running query
    AtomicLong searchUpdateSeq = new AtomicLong(0);

    public void searchPrefixes(String term) {
        Long seq = searchUpdateSeq.incrementAndGet();

        // get the list of callsigns
        List<CallsignEntity> results = null;
        Callsign callsign;

        if (!term.isEmpty()) {
            try {
                callsign = new Callsign(term);
            } catch (IllegalArgumentException e) {
                callsign = Callsign.searchPrefix(term);
            }

            results = callsignDatabase.findMatchingCallsigns(callsign);
        }

        updateCallsignList(seq, results);
    }

    private void updateCallsignList(Long seq, final List<CallsignEntity> results) {
        // If the update is not the most recently requested one, skip it
        if (searchUpdateSeq.get() != seq) return;

        // update UI and make sure it runs in proper thread
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ListView resultView = (ListView)findViewById(R.id.results);
                CallsignElementArrayAdapter adapter = (CallsignElementArrayAdapter)resultView.getAdapter();
                adapter.clear();
                if (results != null) {
                    for(CallsignEntity call: results) {
                        adapter.add(call);
                    }
                }
                adapter.notifyDataSetChanged();
            }
        });
    }

    // http://stackoverflow.com/questions/3349121/how-do-i-use-inputfilter-to-limit-characters-in-an-edittext-in-android
    InputFilter callsignInputFilterBuilder() {
        return new InputFilter() {
            @Override
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                if (source instanceof SpannableStringBuilder) {
                    SpannableStringBuilder sourceAsSpannableBuilder = (SpannableStringBuilder)source;
                    for (int i = end - 1; i >= start; i--) {
                        char currentChar = source.charAt(i);
                        if (!Character.isLetterOrDigit(currentChar) &&
                            !Character.toString(currentChar).equals("/") &&
                            !Character.toString(currentChar).equals("-")) {
                            sourceAsSpannableBuilder.delete(i, i+1);
                        }
                    }
                    return source;
                } else {
                    StringBuilder filteredStringBuilder = new StringBuilder();
                    for (int i = 0; i < end; i++) {
                        char currentChar = source.charAt(i);
                        if (Character.isLetterOrDigit(currentChar) ||
                            Character.toString(currentChar).equals("/") ||
                            Character.toString(currentChar).equals("-")) {
                            filteredStringBuilder.append(currentChar);
                        }
                    }
                    return filteredStringBuilder.toString();
                }
            }
        };
    }

    @Override
    public Optional<GlobalCoordinates> getLocation() {
        if (currentLocation == null) {
            if (!preferences.maidenheadLocation().exists()) {
                return Optional.absent();
            } else {
                return Optional.of(MaidenheadCalculator.fromMaidenhead(preferences.maidenheadLocation().get()));
            }
        }
        return Optional.of(new GlobalCoordinates(currentLocation.getLatitude(), currentLocation.getLongitude()));
    }

    private static final int TWO_MINUTES = 1000 * 60 * 2;

    /** Determines whether one Location reading is better than the current Location fix
     * @param location  The new Location that you want to evaluate
     * @param currentBestLocation  The current Location fix, to which you want to compare the new one
     */
    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (location == null) {
            // Null update is definitely not better
            return false;
        }

        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }

}

interface SearchCallback {
    void searchPrefixes(String term);
}

class SearchFieldChanged implements TextWatcher {
    SearchCallback backlink;
    Timer timer = null;

    SearchFieldChanged(SearchCallback backlink) {
        this.backlink = backlink;
    }

    public void afterTextChanged(final Editable s) {
        if ( timer!=null ) timer.cancel(); // no more tasks can be scheduled..
        timer = new Timer("DelayedInput");
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                backlink.searchPrefixes(s.toString());
            }
        }, 500);
    }
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
    public void onTextChanged(CharSequence s, int start, int before, int count) {}
}
