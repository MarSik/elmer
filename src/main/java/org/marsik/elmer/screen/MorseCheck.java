package org.marsik.elmer.screen;

import java.util.ArrayList;
import java.util.List;

import android.graphics.Color;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.wefika.flowlayout.FlowLayout;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.marsik.elmer.R;
import org.marsik.elmer.morse.MorseEvaluation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import android.support.v4.app.FragmentActivity;

@EActivity(R.layout.morse_check)
public class MorseCheck extends FragmentActivity implements MorseEvaluation.ScoreChanged {
    private static final Logger log = LoggerFactory.getLogger(MorseCheck.class);

    public static final String MESSAGE = "morse.message";
    public static final String GOOD = "morse.good";
    public static final String BAD = "morse.bad";

    @ViewById(R.id.symbols)
    FlowLayout symbols;

    @ViewById
    TextView score;

    @ViewById(R.id.out_of)
    TextView outOf;

    private abstract static class ResponseListener implements View.OnClickListener {
        final int color;
        final int idx;
        final MorseEvaluation statusArray;
        final Boolean status;

        public ResponseListener(int idx, MorseEvaluation statusArray, int color, Boolean status) {
            this.color = color;
            this.idx = idx;
            this.statusArray = statusArray;
            this.status = status;
        }

        @Override
        public void onClick(View v) {
            v.setBackgroundColor(color);
            statusArray.set(idx, status);
            v.setOnClickListener(buildOtherStateListener());
        }

        protected abstract ResponseListener buildOtherStateListener();

    }

    private static class GoodResponseListener extends ResponseListener {
        public GoodResponseListener(int idx, MorseEvaluation statusArray) {
            super(idx, statusArray, Color.GREEN, true);
        }

        @Override
        protected ResponseListener buildOtherStateListener() {
            return new BadResponseListener(idx, statusArray);
        }
    }

    private static class BadResponseListener extends ResponseListener {
        public BadResponseListener(int idx, MorseEvaluation statusArray) {
            super(idx, statusArray, Color.RED, false);
        }

        @Override
        protected ResponseListener buildOtherStateListener() {
            return new GoodResponseListener(idx, statusArray);
        }
    }

    @AfterViews
    public void prepareMessage() {
        ArrayList<String> message = getIntent().getStringArrayListExtra(MESSAGE);
        MorseEvaluation status = new MorseEvaluation(message.size());
        status.addScoreListener(this);

        FlowLayout.LayoutParams layoutParams = new FlowLayout.LayoutParams(
                FlowLayout.LayoutParams.WRAP_CONTENT, FlowLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.setMargins(0, 0, 0, 5);

        for (String s: message) {
            Button button = new Button(this);
            button.setText(s);
            button.setBackgroundColor(Color.DKGRAY);
            button.setOnClickListener(new GoodResponseListener(status.size(), status));
            button.setLayoutParams(layoutParams);

            status.add(null);

            symbols.addView(button);
        }
    }

    @Override
    public void scoreChanged(int good, int bad, int size) {
        score.setText(String.valueOf(good) + " [" + ((100 * good) / size) + "%]");
        outOf.setText(String.valueOf(size));
    }
}
