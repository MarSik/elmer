package org.marsik.elmer.screen;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.NumberPicker;
import com.google.common.base.Joiner;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.ViewById;
import org.androidannotations.annotations.sharedpreferences.Pref;
import org.marsik.elmer.R;
import org.marsik.elmer.morse.Morse;
import org.marsik.elmer.morse.MorsePreferences;
import org.marsik.elmer.morse.MorsePreferences_;

/**
 * TODO
 * Koch mode
 * Farnsworth mode
 * Morse test with selected letters
 * - 5 letter groups
 * - variable letter groups
 * - callsigns
 * Instant recognition practice mode
 * - plays morse char
 * - once it finishes playing it flashes the played letter (big) on screen
 * - repeat...
 */
@EActivity(R.layout.morse_dashboard)
public class MorseDashboard extends FragmentActivity {
    @ViewById(R.id.symbol_speed)
    NumberPicker symbolSpeed;

    @ViewById(R.id.apparent_speed)
    NumberPicker apparentSpeed;

    @ViewById(R.id.pitch)
    NumberPicker pitch;

    @ViewById(R.id.koch_level)
    NumberPicker kochLevel;

    @Pref
    MorsePreferences_ preferences;

    @AfterViews
    public void initViews() {
        symbolSpeed.setMinValue(5);
        symbolSpeed.setMaxValue(30);
        symbolSpeed.setValue(preferences.wpm().get());

        apparentSpeed.setMinValue(1);
        apparentSpeed.setMaxValue(symbolSpeed.getMaxValue());
        apparentSpeed.setValue(preferences.spaceWpm().get());

        pitch.setMinValue(500);
        pitch.setMaxValue(800);
        pitch.setValue(preferences.pitch().get());

        kochLevel.setMinValue(2);
        kochLevel.setMaxValue(Morse.KOCH_ORDER.length);
        kochLevel.setValue(preferences.kochLevel().get());
    }

    @Click(R.id.start_button)
    public void startLesson() {
        savePrefs();

        Intent intent = new Intent(this, MorseSession_.class);
        intent.putExtra(MorseSession.SPEED, symbolSpeed.getValue());
        intent.putExtra(MorseSession.APPARENT_SPEED, apparentSpeed.getValue());
        intent.putExtra(MorseSession.PITCH, pitch.getValue());

        final ArrayList<String> message = new ArrayList<>(Morse.prepareKochMessage(kochLevel.getValue()));
        intent.putStringArrayListExtra(MorseSession.TEXT, message);

        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        savePrefs();
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        savePrefs();
        super.onDestroy();
    }

    private void savePrefs() {
        preferences.edit()
                .kochLevel().put(kochLevel.getValue())
                .wpm().put(symbolSpeed.getValue())
                .spaceWpm().put(apparentSpeed.getValue())
                .pitch().put(pitch.getValue())
                .apply();
    }
}
