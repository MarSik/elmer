package org.marsik.elmer;

import android.app.Application;
import com.joanzapata.iconify.Iconify;
import com.joanzapata.iconify.fonts.FontAwesomeModule;
import org.androidannotations.annotations.EApplication;

@EApplication
public class ElmerApplication extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        Iconify
                .with(new FontAwesomeModule());
    }
}
