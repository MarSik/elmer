<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <packaging>apk</packaging>
    <groupId>org.marsik.elmer</groupId>
    <artifactId>android</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <project.apk.release>3</project.apk.release>

        <!-- use UTF-8 for everything -->
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>

        <maven.compiler.source>1.7</maven.compiler.source>
        <maven.android.target>1.7</maven.android.target>

        <jackson.version>2.6.1</jackson.version>

        <sign.alias>Google Play mars@montik.net</sign.alias>
        <sign.keystore>/home/msivak/.googleplay.jks</sign.keystore>
    </properties>

    <repositories>
        <repository>
            <id>central</id>
            <name>Maven Repository Switchboard</name>
            <layout>default</layout>
            <url>http://repo1.maven.org/maven2</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>

        <repository>
            <id>clojars.org</id>
            <url>http://clojars.org/repo</url>
        </repository>
        
        <repository>
            <id>android-local</id>
            <name>Android support repository</name>
            <url>file://${env.ANDROID_HOME}/extras/android/m2repository</url>
        </repository>
    </repositories>

    <dependencies>
        <dependency>
            <groupId>com.google.android</groupId>
            <artifactId>android</artifactId>
            <version>4.1.1.4</version>
            <scope>provided</scope>
        </dependency>

        <dependency>
            <groupId>com.android.support</groupId>
            <artifactId>support-v4</artifactId>
            <version>22.2.1</version>
            <type>aar</type>
        </dependency>

        <dependency>
            <groupId>com.android.support</groupId>
            <artifactId>gridlayout-v7</artifactId>
            <version>22.2.1</version>
            <type>aar</type>
        </dependency>

        <dependency>
            <groupId>com.wefika</groupId>
            <artifactId>flowlayout</artifactId>
            <version>0.4.1</version>
            <type>aar</type>
        </dependency>

        <dependency>
            <groupId>com.google.guava</groupId>
            <artifactId>guava</artifactId>
            <version>18.0</version>
        </dependency>

        <dependency>
            <groupId>org.androidannotations</groupId>
            <artifactId>androidannotations</artifactId>
            <version>3.3.2</version>
            <scope>provided</scope>
        </dependency>
        <dependency>
            <groupId>org.androidannotations</groupId>
            <artifactId>androidannotations-api</artifactId>
            <version>3.3.2</version>
        </dependency>

        <!-- QR scanner integration - see https://github.com/zxing/zxing/wiki/Scanning-Via-Intent //-->
        <dependency>
            <groupId>com.google.zxing</groupId>
            <artifactId>android-integration</artifactId>
            <version>3.2.1</version>
        </dependency>

        <dependency>
            <groupId>com.squareup.okhttp</groupId>
            <artifactId>okhttp-urlconnection</artifactId>
            <version>2.7.0</version>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-lang3</artifactId>
            <version>3.3.2</version>
        </dependency>

        <!-- Font Awesome //-->
        <dependency>
            <groupId>com.joanzapata.iconify</groupId>
            <artifactId>android-iconify</artifactId>
            <version>2.2.1</version>
            <type>aar</type>
                <!-- There is an issue with iconify being required as JAR and provided
                     as AAR in the dependency //-->
            <exclusions>
                <exclusion>
                    <groupId>com.android.support</groupId>
                    <artifactId>support-v4</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>com.joanzapata.iconify</groupId>
            <artifactId>android-iconify-fontawesome</artifactId>
            <version>2.2.1</version>
            <type>aar</type>
            <exclusions>
                <!-- There is an issue with iconify being required as JAR and provided
                     as AAR in the dependency //-->
                <exclusion>
                    <groupId>com.joanzapata.iconify</groupId>
                    <artifactId>android-iconify</artifactId>
                </exclusion>
            </exclusions>
        </dependency>

        <dependency>
            <groupId>org.apache.commons</groupId>
            <artifactId>commons-math3</artifactId>
            <version>3.6.1</version>
        </dependency>

        <dependency>
            <groupId>com.j256.ormlite</groupId>
            <artifactId>ormlite-android</artifactId>
            <version>4.48</version>
        </dependency>

        <dependency>
            <groupId>org.jdeferred</groupId>
            <artifactId>jdeferred-android</artifactId>
            <version>1.2.4</version>
            <type>apklib</type>
        </dependency>

        <dependency>
            <groupId>org.clojars.ndimiduk</groupId>
            <artifactId>geodesy</artifactId>
            <version>1.1.1</version>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-android</artifactId>
            <version>1.7.21</version>
        </dependency>

        <dependency>
            <groupId>junit</groupId>
            <artifactId>junit</artifactId>
            <version>4.12</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.hamcrest</groupId>
            <artifactId>hamcrest-all</artifactId>
            <version>1.3</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.assertj</groupId>
            <artifactId>assertj-core</artifactId>
            <version>2.4.1</version>
            <scope>test</scope>
        </dependency>

        <dependency>
            <groupId>org.projectlombok</groupId>
            <artifactId>lombok</artifactId>
            <version>1.16.8</version>
            <scope>provided</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.1</version>
                <configuration>
                    <source>${maven.compiler.source}</source>
                    <target>${maven.android.target}</target>
                    <compilerArgs>
                        <arg>-AandroidManifestFile=${project.basedir}/src/main/AndroidManifest.xml</arg>
                    </compilerArgs>
                </configuration>
            </plugin>
            <plugin>
                <groupId>com.simpligility.maven.plugins</groupId>
                <artifactId>android-maven-plugin</artifactId>
                <version>4.4.1</version>
                <configuration>
                    <sdk>
                        <!-- platform as api level (api level 16 = platform 4.1)-->
                        <platform>16</platform>
                    </sdk>
                    <deleteConflictingFiles>true</deleteConflictingFiles>
                    <undeployBeforeDeploy>true</undeployBeforeDeploy>
                    <sign><debug>true</debug></sign>
                    <zipalign>
                        <skip>false</skip>
                        <verbose>true</verbose>
                        <inputApk>${project.build.directory}/${project.artifactId}-${project.version}.apk</inputApk>
                        <outputApk>${project.build.directory}/${project.artifactId}-${project.version}-signed-aligned.apk
                        </outputApk>
                    </zipalign>
                </configuration>
                <extensions>true</extensions>
                <executions>
                    <execution>
                        <id>update-manifest</id>
                        <phase>process-resources</phase>
                        <goals>
                            <goal>manifest-update</goal>
                        </goals>
                        <configuration>
                            <manifest>
                                <versionCode>${project.apk.release}</versionCode>
                                <versionName>${project.version}</versionName>
                            </manifest>
                        </configuration>
                    </execution>
                    <execution>
                        <id>alignApk</id>
                        <phase>package</phase>
                        <goals>
                            <goal>zipalign</goal>
                        </goals>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>release</id>

            <properties>
                <android.release>true</android.release>
            </properties>

            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-jarsigner-plugin</artifactId>
                        <version>1.4</version>
                        <executions>
                            <execution>
                                <id>signing</id>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                                <phase>package</phase>
                                <inherited>true</inherited>
                                <configuration>
                                    <archiveDirectory></archiveDirectory>
                                    <includes>
                                        <include>${project.build.directory}/${project.artifactId}.apk</include>
                                    </includes>
                                    <keystore>${sign.keystore}</keystore>
                                    <storepass>${sign.storepass}</storepass>
                                    <alias>${sign.alias}</alias>
                                    <keypass>${sign.keypass}</keypass>
                                    <arguments>
                                        <argument>-sigalg</argument><argument>MD5withRSA</argument>
                                        <argument>-digestalg</argument><argument>SHA1</argument>
                                    </arguments>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>

                    <plugin>
                        <groupId>com.simpligility.maven.plugins</groupId>
                        <artifactId>android-maven-plugin</artifactId>
                        <configuration>
                            <proguard>
                                <skip>true</skip>
                            </proguard>
                            <sign><debug>false</debug></sign>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>
</project>
